package com.prograd;


import java.util.Calendar;
import java.util.Date;


public class LastSunday {
    
    private final int year ;
    
    public LastSunday(int year) {
        this.year = year ;
    }

    public int[] ofEachMonth() {
        int[] numberOfDays = {31, isLeapYear(year)?29:28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } ;
        int[] sundayDates = new int[12] ;

        for(int month=0; month<12; month++) {
            for (int day=numberOfDays[month]; day>=1; day--) {
                Date date = new Date(year, month, day) ;

                if(date.getDay() == 1) {
                    sundayDates[month] = day ;
                    break;
                }
            }
        }
        return sundayDates;
    }

    private static boolean isLeapYear(int year){
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }}
