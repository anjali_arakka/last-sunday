package com.prograd ;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LastSundayTest {

    @Test
    void should_return_last_sunday_of_each_month_when_year_is_given() {
        LastSunday lastSunday = new LastSunday(2014);

       int[] actualSundays =  lastSunday.ofEachMonth();

       int[] expectedSundays = {26, 23, 30, 27, 25, 29, 27, 31, 28, 26, 30, 28};
       assertThat(actualSundays, is(equalTo(expectedSundays))) ;

    }
}
